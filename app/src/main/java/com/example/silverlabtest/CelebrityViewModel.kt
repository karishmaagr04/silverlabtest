package com.example.silverlabtest

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.silverlabtest.model.CelebrityData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CelebrityViewModel : ViewModel() {

    private val celebrityData by lazy {
        MutableLiveData<CelebrityData>()
    }

    fun getCelebrityData(): LiveData<CelebrityData> = celebrityData

    fun getCelebrityDataResponse() {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val response = withContext(Dispatchers.IO) {
                    RetrofitClientInstance.retrofit.create(CelebritiesService::class.java)
                        .getCelebrityResponse()
                }
                if (response.isSuccessful) {
                    celebrityData.value = response.body()
                }
            } catch (e: Exception) {
                // we can catch exception here
            }
        }
    }
}