package com.example.silverlabtest

import com.example.silverlabtest.model.CelebrityData
import retrofit2.Response
import retrofit2.http.GET

interface CelebritiesService {

    @GET("/bins/1gaa29")
    suspend fun getCelebrityResponse(): Response<CelebrityData>
}