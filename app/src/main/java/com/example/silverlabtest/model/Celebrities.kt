package com.example.silverlabtest.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Celebrities(

    @SerialName("Aarya")
    val aarya: Aarya? = null,

    @SerialName("Cersie")
    val cersie: Cersie? = null,

    @SerialName("Daenerys")
    val daenerys: Daenerys? = null,

    @SerialName("Jaime")
    val jaime: Jaime? = null,

    @SerialName("Jon Snow")
    val jonSnow: JonSnow? = null,

    @SerialName("Samwell")
    val samwell: Samwell? = null,

    @SerialName("Tormund")
    val tormund: Tormund? = null,

    @SerialName("Tyrion")
    val tyrion: Tyrion? = null
)