package com.example.silverlabtest.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CelebrityData(

    @SerialName("cars")
    val cars: Cars? = null,

    @SerialName("celebrities")
    val celebrities: Celebrities? = null
)