package com.example.silverlabtest.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class JonSnow(

    @SerialName("age")
    val age: Int? = null,

    @SerialName("height")
    val height: String? = null,

    @SerialName("photo")
    val photo: String? = null,

    @SerialName("popularity")
    val popularity: Int? = null
)