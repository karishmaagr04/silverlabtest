package com.example.silverlabtest.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Toyota(

    @SerialName("photo")
    val photo: String? = null
)