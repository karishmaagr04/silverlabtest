package com.example.silverlabtest.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Tata(

    @SerialName("photo")
    val photo: String? = null
)