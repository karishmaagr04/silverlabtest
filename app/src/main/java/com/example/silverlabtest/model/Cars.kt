package com.example.silverlabtest.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Cars(

    @SerialName("Honda")
    val honda: Honda? = null,

    @SerialName("Suzuki")
    val suzuki: Suzuki? = null,

    @SerialName("Tata")
    val tata: Tata? = null,

    @SerialName("Toyota")
    val toyota: Toyota? = null
)