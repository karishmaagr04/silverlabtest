package com.example.silverlabtest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.silverlabtest.model.CelebrityData


class MainActivity : AppCompatActivity() {
    private var celebrityViewModel: CelebrityViewModel? = null
    private var celebrityData: CelebrityData? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        celebrityViewModel = ViewModelProviders.of(this).get(CelebrityViewModel::class.java)
        celebrityViewModel?.getCelebrityDataResponse()
        celebrityViewModel?.getCelebrityData()?.observe(this, Observer {
            celebrityData = it
        })

    }
}
